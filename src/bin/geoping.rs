use colored::Colorize;
use serde::{Deserialize, Serialize};
use serde_json;
use std::net::IpAddr;
use structopt::StructOpt;

const EXIT_ERROR_CODE: i32 = 1;

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
enum Response {
    Success(PingResult),
    Error { error: String },
}

#[derive(Serialize, Deserialize, Debug)]
struct PingResult {
    ip: IpAddr,
    is_alive: bool,
    min_rtt: f32,
    avg_rtt: f32,
    max_rtt: f32,
    rtts: Vec<f32>,
    packets_sent: u8,
    packets_received: u8,
    packet_loss: f32,
    from_loc: Location,
}

#[derive(Serialize, Deserialize, Debug)]
struct Location {
    city: String,
    country: String,
    latlon: String,
}

#[derive(Debug, StructOpt)]
#[structopt(
    name = "geoping",
    about = "Ping an IP from multiple locations around the world"
)]
struct Cli {
    /// Output format (shell or json)
    #[structopt(default_value = "shell", short, long)]
    output: String,

    /// IP address or hostname
    address: String,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Cli::from_args();

    // Send the GeoNet API request and grab the response
    let url = format!("https://geonet.shodan.io/api/geoping/{}", args.address);
    let results: Vec<Response> = ureq::get(&url.to_string()).call()?.into_json()?;
    let mut pings: Vec<&PingResult> = results
        .iter()
        .filter_map(|response| match response {
            Response::Success(ping) => Some(ping),
            _ => None,
        })
        .collect::<Vec<_>>();

    if pings.is_empty() {
        println!("{}: Invalid address", "Error".red());
        std::process::exit(EXIT_ERROR_CODE);
    }

    // Sort by the city name of the GeoNet worker so the output looks consistent
    pings.sort_by(|a, b| a.from_loc.city.cmp(&b.from_loc.city));

    // Calculate the min/ max ping times for nicer output
    let mut avg_rtts = pings.iter().map(|ping| ping.avg_rtt).collect::<Vec<f32>>();
    avg_rtts.sort_by(|a, b| a.partial_cmp(b).unwrap());
    let min_rtt = avg_rtts.first().ok_or("No ping results available")?;
    let max_rtt = avg_rtts.last().ok_or("No ping results available")?;

    // Print the results
    for ping in pings.iter() {
        if args.output == "json" {
            println!("{}", serde_json::to_string(*ping)?)
        } else {
            let info = *ping;
            let location = format!("{} ({})", info.from_loc.city, info.from_loc.country).bold();
            if info.is_alive {
                let stats = format!("(min: {:5} ms, max: {:5} ms)", info.min_rtt, info.max_rtt);
                let mut avg_rtt = format!("{:5} ms", info.avg_rtt).white();

                if info.avg_rtt == *min_rtt {
                    avg_rtt = avg_rtt.green();
                } else if info.avg_rtt == *max_rtt {
                    avg_rtt = avg_rtt.red();
                }
                println!("{:30} {:12}   {}", location, avg_rtt, stats.dimmed());
            } else {
                println!("{:30} {}", location, "unresponsive".red());
            }
        }
    }

    Ok(())
}
